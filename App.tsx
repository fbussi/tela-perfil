import React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import "react-native-gesture-handler";
import Login from "./src/pages/Login/index"
import Cadastro from './src/pages/Cadastro';
import Home from './src/pages/Home';
import Categoria from './src/components/Categoria';
import Header from './src/components/Header';

const App=() => {
  return(
    <View>
    <Home/>
    </View>
  )
}

export default App;