import styled from "styled-components/native";

export const Central = styled.View
`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`

export const Esquerda = styled.View
`
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-end;
margin-right: 56px;
`
export const Lado = styled.View
`
display: flex;
flex-direction: row;
justify-content: center;
margin-top: 21px;
`
export const Lado2 = styled.View
`
display: flex;
flex-direction: row;
justify-content: center;
margin-top: 21px;
margin-bottom: 30px;
`
export const Vermelho2 = styled.Text
`
font-size: 18px
color:#A93030
margin-bottom: 20px;
margin-left: 5px;
`
export const Preto = styled.Text
`
font-size: 18px
color:#313131
`

export const Titulo = styled.Text
`
font-size: 30px
font-style: bold
color:#313131
margin-bottom: 20px;
`
export const Italico = styled.Text
`
font-size: 30px
font-style: bold
font-style: italic;
color:#313131
margin-bottom: 20px;
`

export const Linha = styled.View
`
width= 267px;
height= 13px;
background: #C4C4C4;
`