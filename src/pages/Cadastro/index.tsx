import React from "react";
import { View, Text } from "react-native";
import Divider from "../../../node_modules/react-native-divider/index";
import InputTexto from "../../components/InputTexto";
import { Central, Italico, Lado, Lado2, Linha, Preto, Titulo, Vermelho2 } from "./styles";

const Cadastro = ()=> {
    return(
        <>
        <Central>
            <Lado2>
            <Titulo>Bem-vindo ao </Titulo><Italico>Gavetinha</Italico><Titulo>!</Titulo>
            </Lado2>
            <InputTexto nome={"Nome Completo"} param={"João da Silva"}/>
            <InputTexto nome={"CPF"} param={"12345678988"}/>
            <InputTexto nome={"Data de Nascimento"} param={"06/01/1998"}/>
            <InputTexto nome={"Número de Celular"} param={"2199988774"}/>
            <InputTexto nome={"E-mail"} param={"joao@email.com"}/>
            <InputTexto nome={"Senha"} param={"senha"}/>
            <InputTexto nome={"Confirmar senha"} param={"senha"}/>
            <Lado>
                <Preto>Já tem conta?</Preto>
                <Vermelho2>Faça login!</Vermelho2>
            </Lado>
        </Central>
        
        </>
    )
}

export default Cadastro;