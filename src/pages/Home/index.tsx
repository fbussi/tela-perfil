import React from "react";
import { StyleSheet, View, Text } from "react-native";
import CompHeader from "../../components/Header";
import Destaque from "../../components/Destaque";
import Categoria from "../../components/Categoria";
import Itens from "../../components/Itens";
import Menu from "../../components/Menu";
import AppLoading from "expo-app-loading";
import { useFonts,Lato_900Black,} from '@expo-google-fonts/lato';
import { ScrollView } from "react-native-gesture-handler";

  

    

  const Home = ()=> {
        let [fontsLoaded,error] = useFonts({Lato_900Black,})

        if (!fontsLoaded) {
            return <AppLoading/>;
          }
          
    return (
        
        <>
        <View style={styles.margem}>

            <CompHeader />

            <ScrollView>

                <Text style={{ fontFamily: "Lato_900Black", fontSize: 18, marginTop: 45 }}>Destaques</Text>
                <Destaque />

                <Text style={{ fontFamily: "Lato_900Black", fontSize: 18, marginTop: 40 }}>Categorias em destaque</Text>
                <Categoria />

                <Text style={{ fontFamily: "Lato_900Black", fontSize: 18, marginTop: 41 }}>Itens mais vendidos</Text>
                <Itens />

            </ScrollView>

        </View>

        <View>
            <Menu/>
        </View>
        
        </>
         

         
         
        
                
    )

}

const styles=StyleSheet.create({
    margem: {
        marginVertical:21,
        marginLeft:21

    }
})

export default Home;