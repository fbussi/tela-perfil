import styled from "styled-components/native";

export const Central = styled.View
`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`
export const Esquerda = styled.View
`
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-end;
margin-right: 56px;
`

export const BotoesEspaco = styled.View
`
display: flex;
flex-direction: row;
justify-content: space-evenly;
margin-bottom: 20px;
`

export const Lado = styled.View
`
display: flex;
flex-direction: row;
justify-content: center;
`

export const Titulo = styled.Text
`
font-size: 30px
font-style: bold
color:#313131
margin-bottom: 20px;
`

export const Vermelho = styled.Text
`
font-size: 13px
color:#A93030
margin-bottom: 20px;
`
export const Vermelho2 = styled.Text
`
font-size: 18px
color:#A93030
margin-bottom: 20px;
margin-left: 5px;
`
export const Preto = styled.Text
`
font-size: 18px
color:#313131
`
