import React from "react";
import {Text, Image, View, Button} from "react-native";
import Botao from "../../components/Botao";
import BotoesCustom from "../../components/BotoesCustom";
import InputTexto from "../../components/InputTexto/index";
import { Boto2, BotoesEspaco, Central, Esquerda, Lado, Preto, Titulo, Vermelho, Vermelho2 } from "./styles";

const Login = ()=> {
    return(
        <>
        <Central>
            <Titulo>Bem Vindo de Volta!</Titulo>
            <InputTexto nome={"e-mail"} param={"joao@email.com"}/>
            <InputTexto nome={"senha"} param={"senha"}/>
        </Central>
        <Esquerda>
        <Vermelho>Esqueceu a senha?</Vermelho>
        </Esquerda>
        <Central>
            <Botao/>
            <Vermelho>Me mande um codigo SMS</Vermelho>
            <Preto>Ou</Preto>
            <BotoesEspaco>
                <BotoesCustom nome={"Entrar como visitante"}/>
            </BotoesEspaco>
            <Lado>
                <Preto>Não tem conta?</Preto>
                <Vermelho2>Cadastre-se</Vermelho2>
            </Lado>
        </Central>
        </>
    )
}

export default Login;