import styled from "styled-components";

export const Boto = styled.button`
width: 173px;
height: 41px;
border-radius: 10px;
border-width: 0px;
background: #D9D9D9;
margin: 50px 50px 10px 50px;
`
export const Texto = styled.text`
font-size: 25px;
font-style: bolder;
color: #313131;
`