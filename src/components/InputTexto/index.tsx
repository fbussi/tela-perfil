import React from "react";
import {View, Text, TextInput} from "react-native";
import {CaixaTexto, Container, Texto} from "./styles";

export default function InputTexto({nome, param}){
    return(
        <Container>
            <Texto>{nome}</Texto>
            <CaixaTexto placeholder={param}/>
        </Container>
    )
}