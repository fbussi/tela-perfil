import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import AppLoading from "expo-app-loading";
import { useFonts,Lato_400Regular, Lato_300Light,} from '@expo-google-fonts/lato';
import { TouchableOpacity } from "react-native-gesture-handler";

export default function Menu(){
    let [fontsLoaded,error] = useFonts({Lato_400Regular,});

    if (!fontsLoaded) {
        return <AppLoading/>;}
    

        return(
            
                <View style={styles.containermenu}>
                    <TouchableOpacity>
                    <Image source={require('../../assets/BotaoInicio.svg')} style={styles.icone1}/>
                    <Text style={styles.titulo}>Inicio</Text>
                    </TouchableOpacity>

                    <TouchableOpacity>
                    <Image source={require('../../assets/BotaoAlertas.svg')} style={styles.icone2}/>
                    <Text style={styles.titulo}>Alertas</Text>
                    </TouchableOpacity>

                    <TouchableOpacity>
                    <Image source={require('../../assets/BotaoLupa.svg')} style={styles.icone3}/>
                    <Text style={styles.titulo}>Pesquisar</Text>
                    </TouchableOpacity>

                    <TouchableOpacity>
                    <Image source={require('../../assets/BotaoCarrinho.png')} style={styles.icone3}/>
                    <Text style={styles.titulo}>Carrinho</Text>
                    </TouchableOpacity>


                </View>
            
            
        )
        }         

const styles=StyleSheet.create({
    containermenu:{
        backgroundColor:"#690017",
        width:360,
        height:59,
        marginTop:32,
        flexDirection:"row",
        justifyContent:"space-evenly",
        
        
        
    },

    icone1:{
        width:23,
        height:26,
        marginTop:11,
        marginLeft:3
        
    },

    icone2:{
        width:22.7,
        height:25.54,
        marginTop:11,
        marginLeft:8
        
    },

    icone3:{
        width:26,
        height:25,
        marginTop:11,
        marginLeft:15
        
    },


    titulo:{
        fontFamily:"Lato_300Light",
        fontSize:12,
        color:"white",
        textAlign:"center",
    },


})