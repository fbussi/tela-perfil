import React from "react";
import { View, Image, StyleSheet } from 'react-native'
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";

export default function CompHeader(){
    return (
        
        <View style={styles.header}>
            <TouchableOpacity>
                <Image 
                    style={styles.imagem1}
                    source={require('../../assets/fotousuariogrande.png')}/>
            </TouchableOpacity>
            
                <Image
                    style={styles.imagem2}
                    source={require('../../assets/logogavetinha.svg')}/>

            <TouchableOpacity>
                <Image
                    style={styles.imagem3}
                    source={require('../../assets/iconelogout.svg')}/>     
            </TouchableOpacity>
        </View>
        
    )
}

const styles=StyleSheet.create({
    header: {
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",
        marginRight:21,
        
    },
    
    imagem1: {
        width:40,
        height:40,
    borderRadius:20},

    imagem2: {
        width:40,
        height:40,
        },
    
    imagem3: {
        width:25,
        height:25,
    }    
    
});

