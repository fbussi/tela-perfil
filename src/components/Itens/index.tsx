import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import AppLoading from "expo-app-loading";
import {useFonts,Lato_400Regular,Lato_300Light} from '@expo-google-fonts/dev';


export default function Itens(){
    let [fontsLoaded,error] = useFonts({Lato_400Regular,Lato_300Light});

    if (!fontsLoaded) {
        return <AppLoading/>;}

        return(
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

            <TouchableOpacity>
                <View style={styles.mascara}>
                    <Image source={require('../../assets/item1kitinfantil.png')} style={styles.item} />
                    <Text ellipsizeMode='tail' numberOfLines={1} style={styles.nomedoitem}>Kit infantil iniciante bijuteria</Text>
                    <Text style={styles.precodoitem}>R$119,90</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity>
                <View style={styles.mascara}>
                    <Image source={require('../../assets/item2fitametrica.png')} style={styles.item} />
                    <Text ellipsizeMode='tail' numberOfLines={1} style={styles.nomedoitem}>Fita métrica flexível 150cm</Text>
                    <Text style={styles.precodoitem}>R$12,99</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity>
                <View style={styles.mascara}>
                    <Image source={require('../../assets/item3colaquente.png')} style={styles.item}/>
                    <Text ellipsizeMode='tail' numberOfLines={1} style={styles.nomedoitem}>Revolver pistola cola quente</Text>
                    <Text style={styles.precodoitem}>R$28,97</Text>
                </View>
            </TouchableOpacity>


            </ScrollView>
    )
}

const styles=StyleSheet.create({
    mascara:{
        width:137,
        marginRight:16
    },
    
    item:{
        width:137,
        height:97
    },

    nomedoitem:{
        fontFamily:"Lato_400Regular",
        fontSize:13,
        color:"#313131"


    },

    precodoitem:{
        fontFamily:"Lato_300Light",
        fontSize:11,
        color:"#313131"

    },





})