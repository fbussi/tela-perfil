import React from "react";
import { Button, Image, StyleSheet, Text } from "react-native";
import { Boto, Texto } from "./styles";
import styled from "styled-components/native";
export default function BotoesCustom({nome}){
    return(
        <Boto>
            <Texto>{nome}</Texto>
        </Boto>
    )
}