import styled from "styled-components";

export const Boto = styled.button`
width: 108px;
height: 36px;
border-radius: 10px;
border-width: 1px;
border-color: #C4C4C4
background: white;
margin: 10px;
`
export const Texto = styled.text`
font-size: 15px;
color: #313131;
`