import React from "react";
import { View, Text, ImageBackground, StyleSheet,} from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import AppLoading from "expo-app-loading";
import {useFonts,Lato_700Bold,DMSerifDisplay_400Regular,Lato_900Black} from '@expo-google-fonts/dev';

export default function Destaque(){

    let [fontsLoaded,error] = useFonts({Lato_700Bold,DMSerifDisplay_400Regular,Lato_900Black});

    if (!fontsLoaded) {
        return <AppLoading/>;}

    return(
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
            
            <TouchableOpacity style={styles.horizontal}>

                <ImageBackground style={styles.destaque1} source={require('../../assets/imgdestaque1.png')}>

                    <View style={styles.textodestaque}>

                        <Text style={styles.topo}> 10%OFF em itens de Papelaria </Text>
                        <Text style={styles.base}>Semana do Consumidor</Text>

                    </View>
                </ImageBackground>

                <ImageBackground style={styles.destaque1} source={require('../../assets/imgdestaque2.png')}>

                    <View style={styles.textodestaque2}>

                        <Text style={styles.topo2}>Workshop de Xilogravura </Text>
                        <Text style={styles.base2}>em parceria com</Text>
                        <Text style={styles.academia}>ej.academy</Text>
                        <Text style={styles.infos}>Mais informações no site do Gavetinha</Text>

                    </View>
                </ImageBackground>

                <ImageBackground style={styles.destaque1} source={require('../../assets/imgdestaque3.png')}>

                    <View style={styles.textodestaque3}>
                        <Text style={styles.topo3}>A História do Brasil contada{"\n"} através do Artesanato</Text>
                        <Text style={styles.base3}>Cerâmica Marajoara</Text>
                    </View>
                </ImageBackground>


            </TouchableOpacity>
        </ScrollView>
        
    )
}

const styles=StyleSheet.create({
    
    horizontal:{
        flexDirection:"row",
        justifyContent:"space-evenly"
        

    },

    destaque1:{
        width:280,
        height:186,
        marginTop:16,
        marginRight:36

    },

    textodestaque:{
        alignItems:"center",
        marginTop:36

    },

    topo:{
        fontFamily:"DMSerifDisplay_400Regular",
        fontSize:33,
        color:"#EAFE00",
        textAlign:"center",
        width:200,
        height:91,
        lineHeight:28
        
    },

    base:{
        fontFamily:"DMSerifDisplay_400Regular",
        fontSize:21,
        color:"white",
        
    },

    textodestaque2:{
        alignItems:"flex-start",
        marginTop:18,
        marginLeft:15

    },

    topo2:{
        fontWeight:'700',
        fontSize:26,
        lineHeight:25,
        color:'black'
        

    },

    base2:{
        fontWeight:'bold',
        fontSize:14,
        color:'#0C4464'

    },

    academia:{
        fontWeight:'700',
        fontSize:17,
        color:'#FFFFFF'

    },

    infos:{
        fontWeight:'bold',
        fontSize:8,
        color:'#0C4464'

    },

    textodestaque3:{
        alignItems:"flex-end",
        marginTop:89
        
        

    },

    topo3:{
        fontFamily:"Lato_700Bold",
        fontSize:13,
        lineHeight:14,
        writingDirection:"rtl",
        backgroundColor:'rgba(0, 0, 0, 0.58)',
        color:"#C6FF4E",

    },

    base3:{
        fontFamily:"Lato_900Black",
        fontSize:33,
        lineHeight:28,
        writingDirection:"rtl",
        color:"white"

    }



});