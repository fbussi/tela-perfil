import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import AppLoading from "expo-app-loading";
import {useFonts,Lato_700Bold,} from '@expo-google-fonts/dev';

  



export default function Categoria(){
    let [fontsLoaded,error] = useFonts({Lato_700Bold,});

    if (!fontsLoaded) {
        return <AppLoading/>;}
    

        return(
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginTop: 17 }}>

            
                <TouchableOpacity>
                    <View style={styles.categoria1}>
                        <Text style={styles.texto}>Semijoias</Text>
                    </View>
                </TouchableOpacity>


                <TouchableOpacity>
                    <View style={styles.categoria2}>
                        <Text style={styles.texto}>Cerâmica</Text>
                    </View>
                </TouchableOpacity>


                <TouchableOpacity>
                    <View style={styles.categoria3}>
                        <Text style={styles.texto}>Macramê</Text>
                    </View>
                </TouchableOpacity>


                <TouchableOpacity>
                    <View style={styles.categoria4}>
                        <Text style={styles.texto}>Maternidade</Text>
                    </View>
                </TouchableOpacity>


                <TouchableOpacity>
                    <View style={styles.categoria5}>
                        <Text style={styles.texto}>Festa Infantil</Text>
                    </View>
                </TouchableOpacity>


                <TouchableOpacity>
                    <View style={styles.categoria6}>
                        <Text style={styles.texto}>Toy Art</Text>
                    </View>
                </TouchableOpacity>  
             



        

        </ScrollView>
    )
}




const styles=StyleSheet.create({


    texto:{
        fontFamily:"Lato_700Bold",
        fontSize:18,
        color:"white",
        textAlign:"center",
        marginTop:8
    

    },

    categoria1:{
        backgroundColor:"#6B0C15",
        width:122,
        height:39,
        borderRadius:6,
        marginRight:18

    },


    categoria2:{
        backgroundColor:"#A93030",
        width:121,
        height:39,
        borderRadius:6,
        marginRight:18

    },


    categoria3:{
        backgroundColor:"#444C11",
        width:118,
        height:39,
        borderRadius:6,
        marginRight:18

    },


    categoria4:{
        backgroundColor:"#366366",
        width:141,
        height:39,
        borderRadius:6,
        marginRight:18

    },


    categoria5:{
        backgroundColor:"#A57C4D",
        width:141,
        height:39,
        borderRadius:6,
        marginRight:18

    },


    categoria6:{
        backgroundColor:"#6C533B",
        width:95,
        height:39,
        borderRadius:6,
        marginRight:18

    }
})