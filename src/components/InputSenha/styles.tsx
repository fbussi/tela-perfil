import styled from "styled-components/native";

export const Container = styled.View `
width: 288px;
`
export const Texto = styled.Text `
font-size: 20px;
margin-bottom: 5px;
font-style: italic;
`

export const CaixaTexto = styled.TextInput`
height: 32px;
font-size: 18px;
background: #D9D9D9;
border-radius: 7px;
padding: 15px;
` 